# MPS garbage collection in banijipers
<!-- file MPS-GC.md -->

See the [README](README.md) of our `banijipers` project.

Notice that *multi-threaded* garbage collection is **very tricky** (if
you want it to be efficient). We really need to base our work on an
existing garbage collector, the [Ravenbrook
MPS](https://www.ravenbrook.com/project/mps). There are no much other
alternatives, except [Boehm's GC](https://www.hboehm.info/gc/), which
is significantly slower.

Developing a GC, even a single-threaded one, is a lot of work. MPS is
rumored to be good (and many benchmarks show that it can be faster
than Boehm GC) and multi-thread friendly. We know several garbage
collected languages implementations which tried several times to add
efficient mulit-thread GC and in practice failed. Read about the
[GIL](https://en.wikipedia.org/wiki/Global_interpreter_lock), which we
want to avoid in general.

If you are not familiar with garbage collection techniques, read first
Paul Wilson's [Uniprocessor Garbage Collection
Techniques](https://www.cs.rice.edu/~javaplt/311/Readings/wilson92uniprocessor.pdf)
paper. Then read [The GC handbook](http://gchandbook.org/) or at least
Jones and Lins' [Garbage
Collection](https://dl.acm.org/citation.cfm?id=236254) book.

Read carefully the [documentation of Ravenbrook's Memory Pool
System](https://www.ravenbrook.com/project/mps/master/manual/html) and
read it several times.

## MPS and Rust

# ABI compatibility
If we code in Rust we are deeply concerned by ABI compatibility with C (notably because of MPS).

See [Notes on Type Layouts and ABIs in Rust](https://gankro.github.io/blah/rust-layouts-and-abis/)

# Other approaches
See Y.Lin et al. [Rust as a Language for High Performance GC Implementation](http://users.cecs.anu.edu.au/~steveb/pubs/papers/rust-ismm-2016.pdf) ISMM 2016 paper.
